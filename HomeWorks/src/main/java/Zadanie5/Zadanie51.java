package Zadanie5;

public class Zadanie51 {
    public static void main(String[] args) {
        int[] array = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        System.out.println(Element(array, 0));
    }
    public static  int Element(int[] array, int index) {
        if (index >= 0 && index < array.length) {
            return array[index];
        }
        System.out.println("Некорректный индекс");
        return - 1;
    }
}
