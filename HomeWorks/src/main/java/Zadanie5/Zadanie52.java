package Zadanie5;

import java.util.Arrays;

public class Zadanie52 {
    public static void main(String[] args) {
        int[] array = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1 ,20};
        printArray(array);
        Sort(array);
        printArray(array);
    }
    public static void Sort(int[] array) {
        for (int i = 0; i < array.length; i = i + 1) {
            for (int k = i; k < array.length - 1; k = k + 1) {
                if (array[k] < array[k + 1]) {
                    int temp = array[k];
                    array[k] = array[k + 1];
                    array[k + 1] = temp;
                }
            }
        }
    }
    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i = i + 1) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }
}
